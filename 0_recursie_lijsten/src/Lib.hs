module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- | Een functie die de som van een lijst getallen berekent
ex1 :: [Int] -> Int
ex1 [x] = x
ex1 (x:xs)= x + ex1 xs

-- | Een functie die alle elementen van een lijst met 1 ophoogt; bijvoorbeeld [1,2,3] -> [2,3,4].
ex2 :: [Int] -> [Int]
ex2 [x] = [x+1]
ex2 (x:xs) = (x+1 : ex2 xs)
-- | Een functie die alle elementen van een lijst met -1 vermenigvuldigt; bijvoorbeeld [1,-2,3] -> [-1,2,-3].
ex3 :: [Int] -> [Int]
ex3 [x]= [x*(-1)]
ex3 (x:xs) = (x*(-1) : ex3 xs)
-- | Een functie die twee lijsten aan elkaar plakt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1,2,3,4,5,6].   
ex4 :: [Int] -> [Int] -> [Int]
ex4 [] y = y
ex4 (x : xs) y = x : ex4 xs y
-- | Een functie die twee lijsten paarsgewijs bij elkaar optelt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1+4, 2+5, 3+6] oftewel [5,7,9].
ex5 :: [Int] -> [Int] -> [Int]
ex5 [x] [y] = [x+y]
ex5 (x:xs) (y:ys) = ex4 [x + y] ( ex5 xs ys )

-- | Een functie die twee lijsten paarsgewijs met elkaar vermenigvuldigt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1*4, 2*5, 3*6] oftewel [4,10,18]. 
ex6 :: [Int] -> [Int] -> [Int]
ex6 [x] [y] = [x*y]
ex6 (x:xs) (y:ys) = ex4 [x * y] ( ex6 xs ys )

-- | Een functie die de functqies uit opgave 1 en 6 combineert tot een functie die het inwendig prodct uitrekent. Bijvoorbeeld: `ex7 [1,2,3] [4,5,6]` -> 1*4 + 2*5 + 3*6 = 32.
ex7 :: [Int] -> [Int] -> Int
ex7 x y = ex1(ex6 x y)
