{-|
    Module      : RTTL
    Description : Derde checkpoint voor V2DeP: audio-synthesis en ringtone-parsing
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we audio genereren op basis van een abstracte representatie van noten.
    Daarnaast gaan we tekst in het RTTL formaat parsen tot deze abstracte representatie.

    Deze module bevat een main-functie voor het lezen van user-supplied RTTL ringtones en het genereren van de behorende audio.
-}

module Main (main) where

import Data.Maybe (fromMaybe)
import Types (Instrument)
import Instruments (defaultInstrument , defaultSquare , defaultTriangle , pop , twisted , bass , kick , noise)
import Data (ppk)
import IO (playRTTL)

-- TODO Schrijf een main-functie die de gebruiker om een RTTL encoded String vraagt, het `instrumentMenu` print en vervolgens een getal overeenkomstig met een instrument. De string wordt met het gekozen element met `playRTTL` afgespeeld. Als er geen geldig instrument wordt meegegeven wordt `defaultInstrument` gepakt.
-- | the Main function that is used when stack run is used. It asks what instrument you would like and what rttl string. Then it generates the song using play RTTL if possible otherwise it defaults to the default variables.
main :: IO ()
main = do
    putStrLn "Hallo ik ben een programma, wat voor instrument wilt u gebruiken? ( 1 tm 8 ): "
    instrument <- getLine
    putStrLn "Hallo ik ben een programma, wat voor RTTL wilt u afspelen? copy paste hier: "
    rttl <- getLine
    case chooseInstrument (read instrument) of 
        Just i ->  playRTTL i rttl
        Nothing ->  playRTTL defaultInstrument ppk

instrumentMenu :: String
instrumentMenu = unlines [ "1: sine"
                         , "2: square"
                         , "3: triangle"
                         , "4: pop"
                         , "5: twisted"
                         , "6: bass"
                         , "7: kick"
                         , "8: noise"
                         ]

-- TODO Schrijf een functie `chooseInstrument` die een `Int` interpreteert tot een `Maybe Instrument` volgens de tabel hierboven.
-- | Chooses an instrument according to a given number, however I think this could be a lot cleaner, any tips? ( can also return nothing if a invalid number is given.)

chooseInstrument :: Int -> Maybe Instrument
chooseInstrument 8 = Just noise
chooseInstrument 2 = Just defaultSquare
chooseInstrument 3 = Just defaultTriangle
chooseInstrument 4 = Just pop
chooseInstrument 5 = Just twisted
chooseInstrument 6 = Just bass
chooseInstrument 7 = Just kick
chooseInstrument 1 = Just defaultInstrument
chooseInstrument _ = Nothing